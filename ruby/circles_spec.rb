require 'minitest/autorun'
require './circles'


describe 'Position' do
  let(:position_0) { Position.new(1, 1) }
  let(:position_1) { Position.new(1, 1) }
  let(:position_2) { Position.new(2, 2) }

  it 'operator == must be correct (0)' do
    position_0.must_equal position_1
  end

  it 'operator == must be correct (1)' do
    position_0.wont_be :==, position_2
  end

  it 'right! should increse col by 1' do
    position_0.right!.col.must_equal 1 + position_1.col
  end

  it 'right! should increse col by N' do
    position_0.right!(3).col.must_equal 3 + position_1.col
  end

  it 'down! should increse row by 1' do
    position_0.down!.row.must_equal 1 + position_1.row
  end

  it 'down! should increse row by N' do
    position_0.down!(3).row.must_equal 3 + position_1.row
  end

  it 'diagonal! should increse both row and col by 1' do
    position_0.diagonal!.row.must_equal 1 + position_1.row
    position_0.col.must_equal 1 + position_1.col
  end

  it 'diagonal! should increse both row and col by N' do
    position_0.diagonal!(3).row.must_equal 3 + position_1.row
    position_0.col.must_equal 3 + position_1.col
  end
end

describe 'Move' do
  let(:move_0) { Move.new(Position.new(1, 1), Position.new(3, 3)) }
  let(:move_1) { Move.new(Position.new(1, 1), Position.new(3, 3)) }
  let(:move_2) { Move.new(Position.new(1, 1), Position.new(4, 4)) }

  it 'operator == must be correct (0)' do
    move_0.must_equal move_1
  end

  it 'operator == must be correct (1)' do
    move_0.wont_be :==, move_2
  end

  it 'length of diagonal move must be correct' do
    move_2.positions.length.must_equal 1 + (move_2.head.row - move_2.tail.row).abs
  end

  let(:move_3) { Move.new(Position.new(5, 1), Position.new(5, 4)) }

  it 'length of row move must be correct' do
    move_3.positions.length.must_equal 1 + (move_3.head.col - move_3.tail.col).abs
  end

  let(:move_4) { Move.new(Position.new(1, 1), Position.new(4, 1)) }

  it 'length of col move must be correct' do
    move_4.positions.length.must_equal 1 + (move_4.head.row - move_4.tail.row).abs
  end
end

describe Board do
  describe "max length of" do
    let(:board) { Board.new 4, 3 }

    it "row must be correct" do
      board.max_row_length_from(Position.new(3, 0)).must_equal 3
    end

    it "row shorter than max eliminations must be correct" do
      board.max_row_length_from(Position.new(3, 2)).must_equal 2
    end

    it "col must be correct" do
      board.max_col_length_from(Position.new(0, 0)).must_equal 3
    end

    it "col shorter than max eliminations must be correct" do
      board.max_col_length_from(Position.new(2, 0)).must_equal 2
    end

    it "dia must be correct" do
      board.max_dia_length_from(Position.new(0, 0)).must_equal 3
    end

    it "dia shorter than max eliminations must be correct" do
      board.max_dia_length_from(Position.new(2, 2)).must_equal 2
    end
  end

  # describe 'size' do
  #   let(:size) { 10 }
  #   let(:board) { Board.new size, 3 }
  #
  #   it "should be correct" do
  #     board.size.must_equal size
  #   end
  #
  #   it "should match initial number of live stones" do
  #     board.live_stones.length.must_equal (1 + size) * size / 2
  #   end
  # end
  #
  # describe 'stone' do
  #   let(:size) { 10 }
  #   let(:board) { Board.new size, 3 }
  #
  #   it "row can not greater then or eaual to size" do
  #     proc { board.stone(size, 0) }.must_raise NoMethodError
  #   end
  #
  #   it "column can not greater then or eaual to size" do
  #     board.stone(0, size).must_be_nil
  #   end
  # end
end
